#!/usr/bin/python3
import os
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab group configuration based on repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: {}".format(args.metadata_path))
    sys.exit(1)

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# Hack to set correct datatype for the group avatar
gitlab.v4.objects.GroupManager._types = {'avatar': gitlab.types.ImageAttribute}

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a group.yaml file?
    if 'group.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'group.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.safe_load( metadataFile )

    print("Processing " + metadata['name'])
    group = os.path.basename(os.path.normpath(currentPath))

    # Get gitlabGroup
    gitlabGroup = gitlabServer.groups.get(group)

    # Set the name and description
    gitlabGroup.name = metadata['name']
    gitlabGroup.description = metadata['description']

    # Update the settings we require our groups to follow as well
    # We disable mentions to prevent people subscribing every developer to a review
    # Likewise, we disable access requests because those come either from a KDE Developer account or need to go via a Sysadmin ticket
    # To ensure people can use the full power of Gitlab, we permit LFS to be used
    gitlabGroup.lfs_enabled = True
    gitlabGroup.mentions_disabled = True
    gitlabGroup.request_access_enabled = False
    # To ensure developers cannot create projects/groups themselves, we restrict this for our groups as well
    gitlabGroup.project_creation_level = "maintainer"
    gitlabGroup.subgroup_creation_level = "maintainer"

    # Set the group avatar
    groupAvatar = os.path.join( currentPath, 'group.png' )
    print("Group avatar" + groupAvatar)
    gitlabGroup.avatar = open(groupAvatar, 'rb')

    # Finally save all of our changes to the group
    gitlabGroup.save()

# All done!
sys.exit(0)

