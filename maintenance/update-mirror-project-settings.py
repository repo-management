#!/usr/bin/python3
import os
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab configuration for Mirrored projects to conform to our standard settings')
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# The following are the patterns we want to protect
branchPatternsToProtect = []

# Retrieve the Mirrors group...
mirrorGroup = gitlabServer.groups.get('mirrors')

# Start going over the location in question...
for mirrorProject in mirrorGroup.projects.list( include_subgroups=True, iterator=True ):
    # First update project settings to match standard KDE rules
    # To be able to do this we need to convert the above 'GroupProject' into an actual 'Project' instance we can work with
    project = gitlabServer.projects.get(mirrorProject.id, lazy=True)

    # First we update the merge method setting
    project.merge_method = 'ff'
    # As well as the only allow merge if discussions are resolved setting
    project.only_allow_merge_if_all_discussions_are_resolved = True
    # Ensure we cleanup branches on source repositories too
    project.remove_source_branch_after_merge = True
    # Disable request access option for all projects, as we want access control managed by developer account process
    project.request_access_enabled = False

    # Disable various bits of functionality in Gitlab which are not useful for mirrors
    project.security_and_compliance_access_level = 'disabled'
    project.environments_access_level = 'disabled'
    project.model_experiments_access_level = 'disabled'
    project.model_registry_access_level = 'disabled'
    project.feature_flags_access_level = 'disabled'
    project.infrastructure_access_level = 'disabled'
    project.monitor_access_level = 'disabled'
    project.pages_access_level = 'disabled'
    project.issues_access_level = 'disabled'
    project.wiki_access_level = 'disabled'
    project.snippets_access_level = 'disabled'
    project.operations_access_level = 'disabled'
    project.releases_access_level = 'disabled'

    # Disable other bits of functionality as well
    project.service_desk_enabled = False
    project.packages_enabled = False

    # Disable most repository functionality except for LFS support as it is not useful for mirrors
    project.merge_requests_access_level = 'disabled'
    project.forking_access_level = 'disabled'
    project.builds_access_level = 'disabled'
    project.lfs_enabled = True

    # Save the first round of information
    project.save()

    # Start a list of known protection rules
    knownRules = []

    # Go over the protection rules we have and make sure they conform with the above
    for protected in project.protectedbranches.list():
        # Is it one of the ones we're allowed to have?
        if protected.name not in branchPatternsToProtect:
            # As it isn't in the list, get rid of it
            protected.delete()
            # Then go on to the next one
            continue

        # Now we need to make sure the access levels are correct
        if protected.merge_access_levels[0]['access_level'] != gitlab.const.DEVELOPER_ACCESS or protected.push_access_levels[0]['access_level'] != gitlab.const.MAINTAINER_ACCESS:
            # Turns out the Gitlab API does not support updating protection rules (at least not with the Python module)
            # We therefore have to remove these ones as well
            protected.delete()
            # Now go on to the next one
            continue

        # Finally add it to the list of rules we've found so we can keep track
        knownRules.append( protected.name )

    # Now determine which ones we are missing
    missingRules = [ pattern for pattern in branchPatternsToProtect if pattern not in knownRules ]

    # Go over the ones we are missing
    for rule in missingRules:
        # And create them
        project.protectedbranches.create({
            'name': rule,
            'merge_access_level': gitlab.const.DEVELOPER_ACCESS,
            'push_access_level': gitlab.const.MAINTAINER_ACCESS
        })

# All done!
sys.exit(0)

