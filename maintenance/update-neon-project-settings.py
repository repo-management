#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

import os
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab configuration for Neon projects to conform to our standard settings')
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# The following are the patterns we want to protect
branchPatternsToProtect = ['master', '*.*', 'Neon/*']

# Retrieve the Neon group...
neonGroup = gitlabServer.groups.get('neon')

# Start going over the location in question...
for neonProject in neonGroup.projects.list( include_subgroups=True, iterator=True ):
    # First update project settings to match standard KDE rules
    # To be able to do this we need to convert the above 'GroupProject' into an actual 'Project' instance we can work with
    project = gitlabServer.projects.get(neonProject.id, lazy=True)

    # First we update the merge method setting
    project.merge_method = 'ff'
    # As well as the only allow merge if discussions are resolved setting
    project.only_allow_merge_if_all_discussions_are_resolved = True
    # Ensure we cleanup branches on source repositories too
    project.remove_source_branch_after_merge = True
    # Disable request access option for all projects, as we want access control managed by developer account process
    project.request_access_enabled = False
    # Disable various bits of functionality in Gitlab which we don't use and is just distracting
    project.releases_access_level = 'disabled'
    project.environments_access_level = 'disabled'
    #project.model_experiments_access_level = 'disabled'
    project.feature_flags_access_level = 'disabled'
    project.infrastructure_access_level = 'disabled'
    project.monitor_access_level = 'disabled'
    # Disable other bits of functionality we don't use
    project.packages_enabled = False
    project.service_desk_enabled = False
    # Enable CI for the project
    project.builds_access_level = 'enabled'
    # Neon does have a small difference here in that they want debian/.gitlab-ci-neon.yml instead of .gitlab-ci.yml though
    project.ci_config_path = 'debian/.gitlab-ci-neon.yml'

    # Neon does not use a traditional branch setup. The default branch is always considered to be Neon/unstable
    try:
        for branch in project.branches.list( get_all=True ):
            if branch.name == 'Neon/unstable': # only if found, otherwise save() fails
                project.default_branch = 'Neon/unstable'
    except gitlab.exceptions.GitlabListError:
        pass

    # Save the first round of information
    project.save()

    # Now we do the repository protection rules....
    # Start a list of known protection rules
    knownRules = []

    # Go over the protection rules we have and make sure they conform with the above
    for protected in project.protectedbranches.list():
        # Is it one of the ones we're allowed to have?
        if protected.name not in branchPatternsToProtect:
            # As it isn't in the list, get rid of it
            protected.delete()
            # Then go on to the next one
            continue

        # Now we need to make sure the access levels are correct
        if protected.merge_access_levels[0]['access_level'] != gitlab.const.DEVELOPER_ACCESS or protected.push_access_levels[0]['access_level'] != gitlab.const.DEVELOPER_ACCESS:
            # Turns out the Gitlab API does not support updating protection rules (at least not with the Python module)
            # We therefore have to remove these ones as well
            protected.delete()
            # Now go on to the next one
            continue

        # Finally add it to the list of rules we've found so we can keep track
        knownRules.append( protected.name )

    # Now determine which ones we are missing
    missingRules = [ pattern for pattern in branchPatternsToProtect if pattern not in knownRules ]

    # Go over the ones we are missing
    for rule in missingRules:
        # And create them
        project.protectedbranches.create({
            'name': rule,
            'merge_access_level': gitlab.const.DEVELOPER_ACCESS,
            'push_access_level': gitlab.const.DEVELOPER_ACCESS
        })

# All done!
sys.exit(0)

